USE [K2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET IDENTITY_INSERT [dbo].[UsersTest] ON 

INSERT [dbo].[UsersTest] ([ID], [Name], [DateOfBirth], [Married], [Gender]) VALUES (1, N'Johnny', CAST(0x0000A2E100000000 AS DateTime), 1, N'M')
INSERT [dbo].[UsersTest] ([ID], [Name], [DateOfBirth], [Married], [Gender]) VALUES (2, N'Ji Kai', CAST(0x0000A2E100000000 AS DateTime), 1, N'M')
INSERT [dbo].[UsersTest] ([ID], [Name], [DateOfBirth], [Married], [Gender]) VALUES (3, N'Yee Khin', CAST(0x0000A2E100000000 AS DateTime), 1, N'M')
INSERT [dbo].[UsersTest] ([ID], [Name], [DateOfBirth], [Married], [Gender]) VALUES (4, N'Juliana', CAST(0x0000A2E100000000 AS DateTime), 1, N'F')
INSERT [dbo].[UsersTest] ([ID], [Name], [DateOfBirth], [Married], [Gender]) VALUES (5, N'Alex', CAST(0x0000A2E100000000 AS DateTime), 1, N'M')
INSERT [dbo].[UsersTest] ([ID], [Name], [DateOfBirth], [Married], [Gender]) VALUES (6, N'Ziqing', CAST(0x0000A2E100000000 AS DateTime), 1, N'M')
SET IDENTITY_INSERT [dbo].[UsersTest] OFF
