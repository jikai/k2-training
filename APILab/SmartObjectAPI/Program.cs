﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SCSMOC = SourceCode.SmartObjects.Client;
using SCHC = SourceCode.Hosting.Client;
using SCDSMOC = SourceCode.Data.SmartObjectsClient;

namespace SmartObjectAPI
{
    public class Program
    {
        const string SMO_NAME = "UsersTest";
        const string SERVER_NAME = "dlx";
        const int OTHER_PORT = 5555;

        public static void Main(string[] args)
        {
            try
            {
                Program p = new Program();
                //p.PrintAllUsers();
                //p.PrintAllUsersWithFilter();
                //p.PrintAllUsersWCF();
                p.PrintAllUserViaGeneratedClass();
                //p.DirectExecution();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
                else
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        public void PrintAllUsers()
        {
            Console.WriteLine("Executing PrintAllUsers");

            //Create SmartObject Client Server
            SCSMOC.SmartObjectClientServer SmOServer = new SCSMOC.SmartObjectClientServer();
            try
            {
                //get connection string
                string connStr = GetConnectionString();
                Console.WriteLine("Connection string format: {0}", connStr);
                Console.WriteLine("======");

                //Create and open connection to SmartObject Server
                SmOServer.CreateConnection();
                SmOServer.Connection.Open(connStr);

                //Select the target SmartObject
                SCSMOC.SmartObject staffObj = SmOServer.GetSmartObject(SMO_NAME);

                //filter, if required.
                staffObj.Properties["Gender"].Value = "M";
                //Set the method to execute on the SmartObject
                staffObj.MethodToExecute = "GetList";

                //Execute SmO reader
                SCSMOC.SmartObjectReader objReader = SmOServer.ExecuteListReader(staffObj);
                #region print to console
                while (objReader.Read())
                {
                    Console.WriteLine("{0}. {1}, {2}, {3}, {4}",
                                        Convert.ToString(objReader["ID"]),
                                        Convert.ToString(objReader["Name"]),
                                        Convert.ToString(objReader["DateOfBirth"]),
                                        Convert.ToString(objReader["Married"]),
                                        Convert.ToString(objReader["Gender"]));
                }
                #endregion
            }
            #region SmartObject Exception
            catch (SCSMOC.SmartObjectException ex)
            {
                StringBuilder errMsg = new StringBuilder();
                foreach (SCSMOC.SmartObjectExceptionData exData in ex.BrokerData)
                {
                    errMsg.AppendLine("Service: " + exData.ServiceName);
                    errMsg.AppendLine("Service Guid: " + exData.ServiceGuid);
                    errMsg.AppendLine("Serverity: " + exData.Severity.ToString());
                    errMsg.AppendLine("Error Message: " + exData.Message);
                    errMsg.AppendLine("InnerException Message: " + exData.InnerExceptionMessage);
                    errMsg.AppendLine("  -------------------------");
                }
            }
            #endregion
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //dispose the connection.
                if (SmOServer.Connection != null && SmOServer.Connection.IsConnected)
                {
                    SmOServer.Connection.Dispose();
                }
            }
        }
        public void PrintAllUsersWithFilter()
        {
            Console.WriteLine("Executing PrintAllUsersWithFilter");

            //Create SmartObject Client Server
            SCSMOC.SmartObjectClientServer SmOServer = new SCSMOC.SmartObjectClientServer();
            try
            {
                //Create and open connection to SmartObject Server
                SmOServer.CreateConnection();
                SmOServer.Connection.Open(GetConnectionString());

                //Select the target SmartObject
                SCSMOC.SmartObject staffObj = SmOServer.GetSmartObject(SMO_NAME);

                //filter for Gender = M
                SCSMOC.Filters.Filter filOne = new SCSMOC.Filters.Equals(
                                                        new SCSMOC.Filters.PropertyExpression("Gender", SCSMOC.PropertyType.Text),
                                                        new SCSMOC.Filters.ValueExpression("M", SCSMOC.PropertyType.Text)
                                                        );
                //filter for Married = yes
                SCSMOC.Filters.Filter filTwo = new SCSMOC.Filters.Equals(
                                                        new SCSMOC.Filters.PropertyExpression("Married", SCSMOC.PropertyType.Text),
                                                        new SCSMOC.Filters.ValueExpression("yes", SCSMOC.PropertyType.YesNo)
                                                        );
                //AND filer to join the 2 conditions above
                SCSMOC.Filters.And filAnd = new SCSMOC.Filters.And((SCSMOC.Filters.LogicalFilter)filOne, (SCSMOC.Filters.LogicalFilter)filTwo);
                //SmartListMethod on GetList
                SCSMOC.SmartListMethod smoList = staffObj.ListMethods["GetList"];
                //pass the filter in
                smoList.Filter = filAnd;
                //set the MethodToExecute to the SmartListMethod
                staffObj.MethodToExecute = smoList.Name;

                //Execute SmO reader
                SCSMOC.SmartObjectReader objReader = SmOServer.ExecuteListReader(staffObj);
                #region print to console
                while (objReader.Read())
                {
                    Console.WriteLine("{0}. {1}, {2}, {3}, {4}",
                                        Convert.ToString(objReader["ID"]),
                                        Convert.ToString(objReader["Name"]),
                                        Convert.ToString(objReader["DateOfBirth"]),
                                        Convert.ToString(objReader["Married"]),
                                        Convert.ToString(objReader["Gender"]));
                }
                #endregion
            }
            #region SmartObject Exception
            catch (SCSMOC.SmartObjectException ex)
            {
                StringBuilder errMsg = new StringBuilder();
                foreach (SCSMOC.SmartObjectExceptionData exData in ex.BrokerData)
                {
                    errMsg.AppendLine("Service: " + exData.ServiceName);
                    errMsg.AppendLine("Service Guid: " + exData.ServiceGuid);
                    errMsg.AppendLine("Serverity: " + exData.Severity.ToString());
                    errMsg.AppendLine("Error Message: " + exData.Message);
                    errMsg.AppendLine("InnerException Message: " + exData.InnerExceptionMessage);
                    errMsg.AppendLine("  -------------------------");
                }
            }
            #endregion
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //dispose the connection.
                if (SmOServer.Connection != null && SmOServer.Connection.IsConnected)
                {
                    SmOServer.Connection.Dispose();
                }
            }
        }
        public void PrintAllUsersWCF()
        {
            //<endpoint categoryPath="TestEmailProj" smartobjectName="UsersTest" alias="Users" isolationLevel="single" />
            Console.WriteLine("Executing PrintAllUsersWF");

            UsersSvc.UsersTestSvcClient svc = new UsersSvc.UsersTestSvcClient();
            try
            {
                svc.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                svc.Open();

                UsersSvc.UsersTest[] result = svc.UsersTestSvc_GetList(null);
                foreach (UsersSvc.UsersTest item in result)
                {
                    Console.WriteLine("{0}. {1}, {2}, {3}, {4}",
                                          Convert.ToString(item.ID),
                                          Convert.ToString(item.Name),
                                          Convert.ToString(item.DateOfBirth),
                                          Convert.ToString(item.Married),
                                          Convert.ToString(item.Gender));
                }
            }
            finally
            {
                svc.Close();
            }
        }
        public void PrintAllUserViaGeneratedClass()
        {
            Console.WriteLine("Executing PrintAllUserViaGeneratedClass");

            k2.denallix.com.SmartObjects.UsersTest obj = new k2.denallix.com.SmartObjects.UsersTest(GetConnectionString());
            obj.Gender = "F";
            DataTable resultDT = obj.GetList();
            foreach (DataRow row in resultDT.Rows)
            {
                Console.WriteLine("ID={0}", row["ID"].ToString());
                Console.WriteLine("Name={0}", row["Name"].ToString());
                Console.WriteLine("DOB={0}", row["DateOfBirth"].ToString());
                Console.WriteLine("Married={0}", row["Married"].ToString());
                Console.WriteLine("Gender={0}", row["Gender"].ToString());
            }
        }
        public void DirectExecution()
        {
            Console.WriteLine("Executing DirectExecution");

            DataTable resultDT = new DataTable();
            using (SCDSMOC.SOConnection smoConn = new SCDSMOC.SOConnection(SERVER_NAME, OTHER_PORT))
            {
                smoConn.Open();
                smoConn.DirectExecution = true;

                string query = "SELECT * FROM [" + SMO_NAME + "] WHERE [Gender] = 'M'";
                using (SCDSMOC.SOCommand cmd = new SCDSMOC.SOCommand(query, smoConn))
                {
                    using (SCDSMOC.SODataAdapter adapter = new SCDSMOC.SODataAdapter(cmd))
                    {
                        adapter.Fill(resultDT);
                    }
                }
            }

            foreach (DataRow row in resultDT.Rows)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}, {4}",
                                    row["ID"].ToString(),
                                    row["Name"].ToString(),
                                    row["DateOfBirth"].ToString(),
                                    row["Married"].ToString(),
                                    row["Gender"].ToString());
            }
        }
        //public static void GetAssociation()
        //{
        //    //Create SmartObject Client Server
        //    SCSMOC.SmartObjectClientServer SmOServer = new SCSMOC.SmartObjectClientServer();
        //    try
        //    {
        //        //Setup the Connection String
        //        SCHC.BaseAPI.SCConnectionStringBuilder connStr = new SCHC.BaseAPI.SCConnectionStringBuilder();
        //        connStr.Integrated = true;
        //        connStr.IsPrimaryLogin = true;
        //        connStr.Port = Convert.ToUInt32(5555);
        //        connStr.Host = "blackpearl";

        //        //Create and open connection to SmartObject Server
        //        SmOServer.CreateConnection();
        //        SmOServer.Connection.Open(connStr.ConnectionString);

        //        //Select the target SmartObject
        //        SCSMOC.SmartObject myEmployeeObj = SmOServer.GetSmartObject("MyEmployeeClaims");

        //        SCSMOC.AssociationCollection assocList = myEmployeeObj.Associations;
        //        foreach (SCSMOC.Association assoc in assocList)
        //        {
        //            //Console.WriteLine("");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error: {0}", ex.Message);
        //        Console.WriteLine("StackTrace: {0}", ex.StackTrace);
        //    }
        //    finally
        //    {
        //        //dispose the connection.
        //        if (SmOServer.Connection != null && SmOServer.Connection.IsConnected)
        //        {
        //            SmOServer.Connection.Dispose();
        //        }
        //    }
        //}

        #region helper
        private string GetConnectionString()
        {
            SCHC.BaseAPI.SCConnectionStringBuilder connStr = new SCHC.BaseAPI.SCConnectionStringBuilder();
            connStr.Integrated = true;
            connStr.IsPrimaryLogin = true;
            connStr.Port = 5555;
            connStr.Host = "dlx";
            return connStr.ConnectionString;
        }
        #endregion
    }
}
