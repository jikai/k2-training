﻿Imports SCHC = SourceCode.Hosting.Client.BaseAPI
Imports SCSMOC = SourceCode.SmartObjects.Client
Imports SCDSMOC = SourceCode.Data.SmartObjectsClient

Module Module1

    Sub Main()
        'SmartObjectList()
        DirectExecution()
    End Sub

    Sub SmartObjectList()
        Dim connStr As New SCHC.SCConnectionStringBuilder()
        connStr.Host = "blackpearl"
        connStr.Integrated = True
        connStr.IsPrimaryLogin = True
        connStr.Port = 5555

        Dim smoSvr As New SCSMOC.SmartObjectClientServer()

        Try
            smoSvr.CreateConnection()
            smoSvr.Connection.Open(connStr.ConnectionString)

            Dim mySmo As SCSMOC.SmartObject
            mySmo = smoSvr.GetSmartObject("Leave")
            mySmo.Properties("Status").Value = "Approved"
            mySmo.MethodToExecute = "GetList"

            Dim smoResultList As SCSMOC.SmartObjectList
            smoResultList = smoSvr.ExecuteList(mySmo)
            For Each item As SCSMOC.SmartObject In smoResultList.SmartObjectsList
                Console.WriteLine("{0}, {1}, {2}, {3}, {4}",
                                  item.Properties("ID").Value,
                                  item.Properties("Name").Value,
                                  item.Properties("StartDate").Value,
                                  item.Properties("EndDate").Value,
                                  item.Properties("Status").Value)
            Next

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Console.WriteLine(ex.StackTrace)
        Finally
            If (smoSvr.Connection.IsConnected) Then
                smoSvr.Connection.Dispose()
            End If
        End Try
    End Sub

    Sub DirectExecution()
        Dim resultDT As New DataTable

        Using smoConn As New SCDSMOC.SOConnection("blackpearl", 5555)
            smoConn.Open()
            smoConn.DirectExecution = True

            Dim query As String
            query = "SELECT * FROM [Leave] WHERE [Name]='Mike' And [Status]='Approved'"

            Using cmd As New SCDSMOC.SOCommand(query, smoConn)
                Using adapter As New SCDSMOC.SODataAdapter(cmd)
                    adapter.Fill(resultDT)
                End Using
            End Using
        End Using

        For Each item As DataRow In resultDT.Rows
            Console.WriteLine("{0}, {1}, {2}, {3}, {4}",
                                  item("ID"),
                                  item("Name"),
                                  item("StartDate"),
                                  item("EndDate"),
                                  item("Status"))
        Next

    End Sub

End Module
