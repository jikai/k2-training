﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using SCHC = SourceCode.Hosting.Client.BaseAPI;
using SCWFC = SourceCode.Workflow.Client;
using SCSMOC = SourceCode.SmartObjects.Client;
using SCWFM = SourceCode.Workflow.Management;

namespace WorkflowAPI
{
    public class Program
    {
        #region constants
        const string SERVER_NAME = "dlx";
        const int WF_PORT = 5252;
        const int OTHER_PORT = 5555;
        #endregion

        public static void Main(string[] args)
        {
            try
            {
                Program p = new Program();
                ///
                /// SourceCode.Workflow.Client Namespace
                ///
                //p.StartNewProcessInstance();
                //p.OpenWorklist();
                //p.OpenWorklistWithFilter();
                //p.OpenWorklistItem(args[0]);
                //p.ExecuteAction(args[0], args[1]);
                //p.CheckMyOOFStatus();
                //p.SetMyOOFStatus(true);

                ///
                /// K2 Services
                ///
                //p.OpenWorkListWCF();
                //p.OpenWorklistWS();
                //p.OpenWorklistREST();

                ///
                /// SourceCode.Workflow.Management Namespace
                ///
                //p.WorkflowManagementFindAllProcInst();
                p.RetryAllInstancesInErrorProfile();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
                else
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        #region SourceCode.Workflow.Client namespace
        public void StartNewProcessInstance()
        {
            Console.WriteLine("Executing StartNewProcessInstance");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                wfConn.Open(SERVER_NAME); //dlx, IP

                string strDate = DateTime.Now.ToString();

                SCWFC.ProcessInstance procInst = wfConn.CreateProcessInstance(@"TestEmailProj\Process1");

                procInst.DataFields["Message"].Value = String.Format("This is a test at time: {0}", strDate);

                procInst.Folio = string.Format("TestCreateProcInst-{0}", strDate);

                wfConn.StartProcessInstance(procInst, true); //run synchronously

                Console.WriteLine("Folio = {0}", procInst.Folio);

                #region Get next serial number and Show Actions Available
                string nextSN = GetNextSerialNo(procInst.ID);
                SCWFC.WorklistItem newItem = wfConn.OpenWorklistItem(nextSN);

                Console.Write("Actions Available:");
                foreach (SCWFC.Action act in newItem.Actions)
                {
                    Console.Write(act.Name + ",");
                }
                Console.WriteLine();
                #endregion

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error {0}", ex.Message);
                Console.WriteLine("StackTrace {0}", ex.StackTrace);
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        public void OpenWorklist()
        {
            Console.WriteLine("Executing OpenWorklist");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                wfConn.Open(SERVER_NAME);
                SCWFC.Worklist myWorkList = wfConn.OpenWorklist();
                foreach (SCWFC.WorklistItem wlItem in myWorkList)
                {
                    Console.WriteLine("{0} - {1} - {2}",
                                                wlItem.SerialNumber,
                                                wlItem.ProcessInstance.Folio,
                                                wlItem.Status);

                    foreach (SCWFC.Action action in wlItem.Actions)
                    {
                        Console.Write(action.Name + ",");
                    }
                    Console.WriteLine();
                }
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        public void OpenWorklistWithFilter()
        {
            Console.WriteLine("Executing OpenWorklistWithFilter");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            SCWFC.WorklistCriteria wlCrit = new SCWFC.WorklistCriteria();

            try
            {
                wfConn.Open(SERVER_NAME);

                #region filter
                wlCrit.Platform = "ASP";
                //ProcessName is like '%Process1'
                wlCrit.AddFilterField(SCWFC.WCField.ProcessName,
                                        SCWFC.WCCompare.Like,
                                        "%Process1");

                // 0= available, 1= open, 2 = allocated, 3=sleep
                wlCrit.AddFilterField(SCWFC.WCLogical.And,
                                        SCWFC.WCField.WorklistItemStatus,
                                        SCWFC.WCCompare.Equal,
                                        1);
                SCWFC.Worklist myWorkList = wfConn.OpenWorklist(wlCrit);
                #endregion

                foreach (SCWFC.WorklistItem wlItem in myWorkList)
                {
                    Console.WriteLine("{0} - {1} - {2}",
                                                wlItem.SerialNumber,
                                                wlItem.ProcessInstance.Folio,
                                                wlItem.Status);
                }
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        public void OpenWorklistItem(string SN)
        {
            Console.WriteLine("Executing OpenWorklistItem");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                wfConn.Open(SERVER_NAME);
                SCWFC.WorklistItem wlItem = wfConn.OpenWorklistItem(SN);
                foreach (SCWFC.Action action in wlItem.Actions)
                {
                    Console.Write(action.Name + ",");
                }
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        public void ExecuteAction(string SN, string action)
        {
            Console.WriteLine("Executing ExecuteAction");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                wfConn.Open(SERVER_NAME);
                SCWFC.WorklistItem wlItem = wfConn.OpenWorklistItem(SN);

                wlItem.Actions[action].Execute();
                Console.WriteLine("Action '{0}' executed!", action);
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        public void CheckMyOOFStatus()
        {
            Console.WriteLine("Executing CheckMyOOFStatus");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                wfConn.Open(SERVER_NAME);

                SCWFC.UserStatuses oStatus = wfConn.GetUserStatus();
                switch (oStatus)
                {
                    case SCWFC.UserStatuses.Available:
                        Console.WriteLine("Status = Available");
                        break;

                    case SCWFC.UserStatuses.None:
                        Console.WriteLine("Status = None");
                        break;

                    case SCWFC.UserStatuses.OOF:
                        Console.WriteLine("Status = Out-Of-Office");
                        break;
                }
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        public void SetMyOOFStatus(bool OOF)
        {
            Console.WriteLine("Executing SetMyOOFStatus");

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                wfConn.Open(SERVER_NAME);

                if (OOF)
                {
                    //Create a share
                    SCWFC.WorklistShare wlShare = new SCWFC.WorklistShare();
                    wlShare.ShareType = SCWFC.ShareType.OOF;
                    wlShare.StartDate = DateTime.Now;
                    wlShare.EndDate = DateTime.Now.AddDays(1);

                    //Forward destination
                    SCWFC.WorkType wtShare = new SCWFC.WorkType("normal");
                    wtShare.WorklistCriteria.Platform = "ASP";
                    wtShare.Destinations.Add(new SCWFC.Destination(@"K2:denallix\mike", SCWFC.DestinationType.User));

                    //Exception destination
                    //SCWFC.WorkTypeException wtExp = new SCWFC.WorkTypeException("except");
                    //wtExp.WorklistCriteria.Platform = "ASP";
                    //wtExp.WorklistCriteria.AddFilterField(SCWFC.WCField.ProcessName, SCWFC.WCCompare.Equal, "ExpenseReview");
                    //wtExp.Destinations.Add(new SCWFC.Destination(@"K2:denallix\codi", SCWFC.DestinationType.User));
                    //wtShare.WorkTypeExceptions.Add(wtExp);

                    //add to worklistshare
                    wlShare.WorkTypes.Add(wtShare);
                    //remove all existing share
                    wfConn.UnShareAll();
                    //add WorklistShare
                    wfConn.ShareWorkList(wlShare);
                    //set user to be OOF
                    wfConn.SetUserStatus(SCWFC.UserStatuses.OOF);
                }
                else
                {
                    wfConn.UnShareAll();
                    wfConn.SetUserStatus(SCWFC.UserStatuses.Available);
                }
            }
            finally
            {
                wfConn.Dispose();
            }
        }
        #endregion
        #region K2 Services
        public void OpenWorkListWCF()
        {
            Console.WriteLine("Executing OpenWorkListWCF");

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            basicHttpBinding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            basicHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress endpoint = new EndpointAddress("http://k2.denallix.com/k2services/WCF.svc/Worklist");

            K2WCF.WorklistNavigationServiceClient svc = new K2WCF.WorklistNavigationServiceClient(basicHttpBinding, endpoint);
            svc.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            //svc.ChannelFactory.Credentials.Windows.ClientCredential = new System.Net.NetworkCredential(strLogin, strPassword, strDomain);

            try
            {
                svc.Open();

                /* actDataField = Activity level data field
                 * actXmlField = Activity level xml field
                 * piDataField = Process Instance level data field
                 * piXmlField = Process Instance level xml field
                 */
                K2WCF.WorklistItem[] worklist = svc.OpenWorklist(false, false, false, false);
                foreach (K2WCF.WorklistItem item in worklist)
                {
                    Console.WriteLine("{0}, {1}, {2}", item.SerialNumber, item.ProcessInstance.Folio, item.Status.ToString());
                }
            }
            catch (Exception ex)
            {
                svc.Abort();
                throw ex;
            }
            finally
            {
                if (svc != null)
                    svc.Close();
            }
        }
        public void OpenWorklistWS()
        {
            Console.WriteLine("Executing OpenWorklistWS");

            K2WS.WS svc = new K2WS.WS();
            try
            {
                svc.UseDefaultCredentials = true;
                K2WS.WorklistItem[] worklist = svc.OpenWorklist(false);
                foreach (K2WS.WorklistItem item in worklist)
                {
                    Console.WriteLine("{0}, {1}, {2}", item.SerialNumber, item.ProcessInstance.Folio, item.Status);
                }
            }
            catch (Exception ex)
            {
                svc.Abort();
                throw ex;
            }
            finally
            {
                svc.Dispose();
            }
        }
        public void OpenWorklistREST()
        {
            Console.WriteLine("Executing OpenWorklistREST");

            RestSharp.RestClient svc = new RestSharp.RestClient("http://k2.denallix.com/k2services/rest.svc");
            svc.Authenticator = new RestSharp.NtlmAuthenticator(System.Net.CredentialCache.DefaultCredentials);
            //svc.Authenticator = new RestSharp.SimpleAuthenticator("username", "denallix\anthony", "password", "K2pass!");
                
            RestSharp.RestRequest req = new RestSharp.RestRequest("worklist/items", RestSharp.Method.GET);

            RestSharp.IRestResponse response = svc.Execute(req);

            K2ServiceRestObjects.Worklist.Worklist worklist = Newtonsoft.Json.JsonConvert.DeserializeObject<K2ServiceRestObjects.Worklist.Worklist>(response.Content);

            foreach (K2ServiceRestObjects.Worklist.WorklistItem item in worklist)
            {
                Console.WriteLine("{0} - {1}, {2} ", item.SerialNumber, item.ProcessInstance.Folio, item.Status);
                Console.Write("Actions: ");
                foreach (K2ServiceRestObjects.Worklist.Action action in item.Actions)
                {
                    Console.Write(action.Name);
                }
                Console.WriteLine();
            }
        }
        #endregion
        #region SourceCode.Workflow.Management namespace
        public void WorkflowManagementFindAllProcInst()
        {
            Console.WriteLine("Executing WorkflowManagementFindAllProcInst");

            SCWFM.WorkflowManagementServer server = new SCWFM.WorkflowManagementServer(SERVER_NAME, OTHER_PORT);
            try
            {
                server.Open();

                //SCWFM.Criteria.WorklistCriteriaFilter crit = new SCWFM.Criteria.WorklistCriteriaFilter();
                //SCWFM.WorklistItems allWlItems = server.GetWorklistItems(crit);
                //foreach (SCWFM.WorklistItem wlItem in allWlItems)
                //{
                //    Console.WriteLine("{0} - {1} - {2} - {3} - processStatus {4}", wlItem.ProcInstID, wlItem.Folio, wlItem.ActivityName, wlItem.Destination, wlItem.ProcessInstanceStatus.ToString());
                //}

                SCWFM.Criteria.ProcessInstanceCriteriaFilter procInstCrit = new SCWFM.Criteria.ProcessInstanceCriteriaFilter();
                SCWFM.ProcessInstances allProcInst = server.GetProcessInstancesAll(procInstCrit);
                foreach (SCWFM.ProcessInstance inst in allProcInst)
                {
                    Console.WriteLine("Folio={0}, Status={1}", inst.Folio, inst.Status);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error {0}", ex.Message);
                Console.WriteLine("StackTrace {0}", ex.StackTrace);
            }
            finally
            {
                if (server.Connection != null && server.Connection.IsConnected)
                {
                    server.Connection.Dispose();
                }
            }
        }
        public void RetryAllInstancesInErrorProfile()
        {
            Console.WriteLine("Executing RetryAllInstancesInErrorProfile");
            SCWFM.WorkflowManagementServer server = new SCWFM.WorkflowManagementServer(SERVER_NAME, OTHER_PORT);
            try
            {
                server.Open();

                //get "All" error profile
                SCWFM.ErrorProfile errorProfile = server.GetErrorProfile("All");
                //load the errors list
                SCWFM.ErrorLogs errLogList = server.GetErrorLogs(errorProfile.ID);
                Console.WriteLine("Error logs found: {0}", errLogList.Count);
                //print the error and retry
                foreach(SCWFM.ErrorLog errLog in errLogList)
                {
                    Console.WriteLine("Retrying {0}, {1}, {2}, {3}", errLog.ProcessName, errLog.Folio, errLog.ErrorDate.ToShortDateString(), errLog.Description);
                    server.RetryError(errLog.ProcInstID, errLog.ID, "denallix\\bob");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error {0}", ex.Message);
                Console.WriteLine("StackTrace {0}", ex.StackTrace);
            }
            finally
            {
                if (server.Connection != null && server.Connection.IsConnected)
                {
                    server.Connection.Dispose();
                }
            }
        }
        #endregion

        #region helpers
        private string GetNextSerialNo(int procInstID)
        {
            SCSMOC.SmartObjectClientServer SmOServer = new SCSMOC.SmartObjectClientServer();
            string nextSN = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(1000);

                //Setup the Connection String
                SCHC.SCConnectionStringBuilder connStr = new SCHC.SCConnectionStringBuilder();
                connStr.Integrated = true;
                connStr.IsPrimaryLogin = true;
                connStr.Port = 5555;
                connStr.Host = SERVER_NAME;

                //Create and open connection to SmartObject Server
                SmOServer.CreateConnection();
                SmOServer.Connection.Open(connStr.ConnectionString);

                SCSMOC.SmartObject actInstDest = SmOServer.GetSmartObject("Activity_Instance_Destination");

                SCSMOC.Filters.Filter smoFilter = new SCSMOC.Filters.Equals(
                                                        new SCSMOC.Filters.PropertyExpression("ProcessInstanceID", SCSMOC.PropertyType.Text),
                                                        new SCSMOC.Filters.ValueExpression(procInstID, SCSMOC.PropertyType.Number)
                                                        );

                SCSMOC.SmartListMethod smoList = actInstDest.ListMethods["List"];
                smoList.Filter = smoFilter;
                actInstDest.MethodToExecute = smoList.Name;

                SCSMOC.SmartObjectReader objReader = SmOServer.ExecuteListReader(actInstDest);
                while (objReader.Read())
                {
                    nextSN = string.Format("{0}_{1}",
                                        procInstID,
                                        Convert.ToString(objReader["Activity Instance Destination ID"]));
                    Console.WriteLine("New SN={0}", nextSN);
                }
            }
            finally
            {
                if (SmOServer.Connection != null && SmOServer.Connection.IsConnected)
                {
                    SmOServer.Connection.Dispose();
                }
            }
            return nextSN;
        }
        #endregion
    }
}
