﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ExecuteK2LearningPrequisite.CourseConfig;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace ExecuteK2LearningPrequisite
{
    public class Program
    {
        private string _xmlConfigFile = string.Empty;
        private string _ftpUserID = string.Empty;
        private string _ftpUserPw = string.Empty;
        private string _ftpUrlFormat = string.Empty;
        private string _extractDir = string.Empty;

        public static void Main(string[] args)
        {
            try
            {
                Program p = new Program();
                p.Start();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        public Program()
        {
            this._xmlConfigFile = ConfigurationManager.AppSettings["courseConfigXML"];
            this._ftpUserID = ConfigurationManager.AppSettings["ftpUserID"];
            this._ftpUserPw = ConfigurationManager.AppSettings["ftpUserPw"];
            this._ftpUrlFormat = ConfigurationManager.AppSettings["ftpUrlFormat"];
            this._extractDir = ConfigurationManager.AppSettings["extractDir"];
        }

        public void Start()
        {
            K2LearningPrerequisite req = LoadConfig();
            CreateExtractDirectory();
            //req.Course[0].GetModuleRows()[0]
            foreach(K2LearningPrerequisite.ModuleRow row in req.Course[0].GetModuleRows())
            {
                string ftpUrl = string.Format(this._ftpUrlFormat, row.code);
                string filePath = DownloadFTPFile(ftpUrl);
                ExtractFile(filePath);
                
                //ExecuteBatchJob(row.code);
                
                //clean up
                Console.Write("Removing {0}...", filePath);
                File.Delete(filePath);
                Console.WriteLine("Done");
            }
        }

        private K2LearningPrerequisite LoadConfig()
        {
            K2LearningPrerequisite req = null;
            XmlSerializer serializer = new XmlSerializer(typeof(K2LearningPrerequisite));
            using (TextReader reader = new StreamReader(this._xmlConfigFile))
            {
                req = (K2LearningPrerequisite)serializer.Deserialize(reader);
            }
            return req;
        }

        private string DownloadFTPFile(string ftpUrl)
        {
            Console.WriteLine("Downloading {0}...", ftpUrl);
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpUrl);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(this._ftpUserID, this._ftpUserPw);

            string tmpFilePath = Path.GetTempPath() + "/" + Path.GetFileName(ftpUrl);
            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {

                using (Stream responseStream = response.GetResponseStream())
                {
                    using (FileStream fs = File.Create(tmpFilePath))
                    {
                        responseStream.CopyTo(fs);
                    }
                }
            }
            Console.WriteLine("Downloaded to {0}...", tmpFilePath);
            return tmpFilePath;
        }

        private void CreateExtractDirectory()
        {
            if (Directory.Exists(this._extractDir)) return;
            Directory.CreateDirectory(this._extractDir);
        }

        private void ExtractFile(string filePath)
        {
            Console.Write("Extracting file {0}...", filePath);
            using (Process process = new Process())
            {
                process.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
                process.StartInfo.FileName = "unrar.exe";
                process.StartInfo.Arguments = string.Format("x -y \"{0}\" \"{1}\"", filePath, this._extractDir);
                process.Start();
                process.WaitForExit();
            }
            Console.WriteLine("Done");
        }

        private void ExecuteBatchJob(string code)
        {
            //TODO: copy StartBatchJob.bat to the folder.
            //TODO: cannot handle the last pause after the batch job completed
            Console.Write("Executing batch job for {0}...", code);
            using (Process process = new Process())
            {
                process.StartInfo.WorkingDirectory = this._extractDir + "\\" + code + "\\Setup\\";
                process.StartInfo.FileName = "SetupPreRequisites.bat";
                process.Start();
                process.WaitForExit();
            }
            Console.WriteLine("Done");
        }
    }
}
